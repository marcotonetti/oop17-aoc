package aoc.view.gameview;

import java.io.IOException;
import aoc.controller.Controller;
import aoc.view.menu.controller.MainWindow;
import aoc.view.menu.controller.Utils;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class GameViewArcadeController extends AbstractGameView{

    @FXML
    private Label scoresLabel;

    /**
     * An empty constructor.
     */
    public GameViewArcadeController() {

    }

    /**
     * The method that sets the label.
     * @param index
     *          scores number
     */
    public void setLabel(final int index) {
        this.scoresLabel.setText(String.valueOf(index));
    }

    /**
     * Getter that picks the canvas of the class.
     * @return canvas
     */
    public Canvas getCanvas() {
        return this.canvas;
    }

    @Override
    public void lost() {
        Controller.get().end();
        Stage window = new Stage();
        window.setWidth(Utils.STAGE_WIDTH/2);
        window.setHeight (Utils.STAGE_HEIGHT/2);
        window.setResizable(false);
        window.setTitle("Finish Game");
        window.setOnCloseRequest(e -> {
            window.close();
            try {
                sf.setScene("Highscores.fxml");
                Scene scene = sf.getScene();
                MainWindow.getSingleton().getWindow().setScene(scene);
                MainWindow.getSingleton().getWindow().show();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        try {
            sf.setScene("Finish.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = sf.getScene();
        window.setScene(scene);
        window.show();
    }
}
