package aoc.view.gameview;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;

/**
 * This class extends AbstractGameView and has a method to set the label 
 * in order to show the level the user is playing.
 *
 */
public class GameViewStoryController extends AbstractGameView {
    /**
     * A label that shows the level the user is playing.
     */
    @FXML
    protected Label numberLevel;
    /**
     * An empty constructor.
     */
    public GameViewStoryController() {

    }

    /**
     * The method that sets the label.
     * @param index
     *          level's number
     */
    public void setLabel(final int index) {
        this.numberLevel.setText(String.valueOf(index));
    }

    /**
     * Getter that picks the canvas of the class.
     * @return canvas
     */
    public Canvas getCanvas() {
        return this.canvas;
    }

}
