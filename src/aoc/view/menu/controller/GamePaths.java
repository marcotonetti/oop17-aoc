package aoc.view.menu.controller;

/**
 * Class that collects all the paths constants.
 *
 */
public final class GamePaths {

    /**
     * Constant that gets the system separator.
     */
    public static final String SEPARATOR = "/";

    /**
     * Path to get the images folder.
     */
    public final static String IMAGES_FOLDER = SEPARATOR + "images" + SEPARATOR;

    /**
     * Path to get the mother image.
     */
    public final static String IMAGES_MOTHER_FOLDER = IMAGES_FOLDER + "mother" + GamePaths.SEPARATOR;

    /**
     * Path to get the children images.
     */
    public final static String IMAGES_CHILD_FOLDER = IMAGES_FOLDER + "child" + GamePaths.SEPARATOR;

    /**
     * Path to get the slipper image.
     */
    public final static String IMAGES_SLIPPER_FOLDER = IMAGES_FOLDER + "slipper" + GamePaths.SEPARATOR;

    /**
     * Path to get the background image.
     */
    public final static String IMAGES_BACKGROUND_FOLDER = IMAGES_FOLDER + "background" + GamePaths.SEPARATOR;

    /**
     * Path to get the FXML folder.
     */
    public final static String FXML_PATH = SEPARATOR + "aoc" + SEPARATOR + "view" + SEPARATOR + "menu" + SEPARATOR + "scenes" + SEPARATOR;
    
    /**
     * Path to get the sounds.
     */
    public final static String SOUND_FOLDER = GamePaths.SEPARATOR + "sounds"  + GamePaths.SEPARATOR;
    
}
