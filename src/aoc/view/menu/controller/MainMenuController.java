package aoc.view.menu.controller;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;;

/**
 * The controller class for the MainMenu scene.
 * 
 *
 */
public class MainMenuController extends BasicController  {

	/**
	 * Set the SelectMode scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void playAction(final ActionEvent event) throws IOException {
		sf.setScene("SelectMode.fxml");
		this.showScene(event);
	}
	
	/**
	 * Set the Option scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void optionAction(final ActionEvent event) throws IOException {
		sf.setScene("Option.fxml");
		this.showScene(event);
	}
	
	/**
	 * Set the Credits scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void creditsAction(final ActionEvent event) throws IOException {
		sf.setScene("Credits.fxml");
		this.showScene(event);
	}
	
	/**
	 * Set the Highscores scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void highScoreAction(final ActionEvent event) throws IOException {
		sf.setScene("Highscores.fxml");
		this.showScene(event);
	}
	
	/**
	 * Set the Guide scene.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void playGuide(final ActionEvent event) throws IOException {
		sf.setScene("Guide.fxml");
		this.showScene(event);
	}
	
	/**
	 * Close the application.
	 * 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void exit(final ActionEvent event) {
		System.exit(0);
	}

}
