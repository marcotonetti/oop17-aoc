package aoc.view.menu.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import aoc.controller.Controller;
import aoc.model.entity.slipper.Projectile;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * The controller class for the Pause scene.
 *
 */
public class PauseController extends BasicController implements Initializable {

    private static final List<Button> slipperButtonList = new ArrayList<>();

    @FXML
    Button slipper1;
    @FXML
    Button slipper2;
    @FXML
    Button slipper3;
    @FXML
    Button backToGame;

    /**
     * Set the selected slipper.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void setSlipper(final ActionEvent event) throws IOException {
        Button button = (Button) event.getSource();
        String name = button.getText();
        for (int i = 0; i  < slipperButtonList.size(); i++) {
            slipperButtonList.get(i).setDisable(false);
        }
        button.setDisable(true);
        switch (name) { 
        case "Tiro Triplo": 
            Controller.get().setSlipper(Projectile.TRIPLE_SLIPPER);
            break; 
        case "Tiro Perforante": 
            Controller.get().setSlipper(Projectile.PIERCING_SLIPPER);
            break; 
        case "Tiro Esplosivo": 
            Controller.get().setSlipper(Projectile.EXPLOSIVE_SLIPPER);
            break; 
        default:
            break;
        }
    }

    /**
     * back to game.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void backToGame(final ActionEvent event) throws IOException {
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
        Controller.get().proceed();
    }

    /**
     * back to main menu.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void backToMenu(final ActionEvent event) throws IOException {
        MainWindow.getSingleton().click();
        MainWindow.getSingleton().getSoundManager().clickBackToMainMenu();
        try {
            sf.setScene("MainMenu.fxml");
            Scene scene = sf.getScene();
            MainWindow.getSingleton().getWindow().setScene(scene);
            MainWindow.getSingleton().getWindow().show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.close();
        Controller.get().end();
    }

    /**
     * Set the MainMenu scene.
     * 
     * @param event
     * @throws IOException
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        slipperButtonList.add(slipper1);
        slipperButtonList.add(slipper2);
        slipperButtonList.add(slipper3);
    }

}
