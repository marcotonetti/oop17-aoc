package aoc.view.menu.controller;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * Implementation of SoundManager.
 *
 */
public class SoundManagerImpl implements SoundManager {

    private static final String path = GamePaths.SOUND_FOLDER + "Rave.wav";
    private final static Media Rave = new Media(SoundManagerImpl.class.getResource(path).toString());
    private static final MediaPlayer MUSIC = new MediaPlayer(Rave);

    private static final String path2 = GamePaths.SOUND_FOLDER + "DefenseLine.mp3";
    private static final Media DefenseLine = new Media(SoundManagerImpl.class.getResource(path2).toString());
    private static final MediaPlayer GAMEMUSIC = new MediaPlayer(DefenseLine);

    private static final String path3 = GamePaths.SOUND_FOLDER + "Click.wav";
    private static final Media Click = new Media(SoundManagerImpl.class.getResource(path3).toString());
    private static final MediaPlayer CLICK = new MediaPlayer(Click);


    /**
     * Constructor that sets the default values.
     */
    public SoundManagerImpl() {
        super();
        MUSIC.setCycleCount(MediaPlayer.INDEFINITE);
        MUSIC.setVolume(0.1);
        MUSIC.setAutoPlay(true);
        GAMEMUSIC.setCycleCount(MediaPlayer.INDEFINITE);
    }

    @Override
    public final double getVolume() {
        return MUSIC.getVolume();
    }

    @Override
    public final void setVolume(final double volume) {
        MUSIC.setVolume(volume);
        CLICK.setVolume(volume);
        GAMEMUSIC.setVolume(volume);
    }

    @Override
    public final void click() {
        CLICK.stop();
        CLICK.play();
        MUSIC.setCycleCount(MediaPlayer.INDEFINITE);
    }

    @Override
    public final void clickPlay() {
        MUSIC.stop();
        GAMEMUSIC.play();
        GAMEMUSIC.setCycleCount(MediaPlayer.INDEFINITE);
    }

    @Override
    public final void clickBackToMainMenu() {
        GAMEMUSIC.stop();
        MUSIC.play();
        MUSIC.setCycleCount(MediaPlayer.INDEFINITE);
    }
}
