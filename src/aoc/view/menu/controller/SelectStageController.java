package aoc.view.menu.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import aoc.controller.Controller;
import aoc.controller.datamanager.DataManager;
import aoc.view.gameview.AbstractGameView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * The controller class for the SelectStage scene.
 *
 */
public class SelectStageController extends BasicController implements Initializable {

    private static final List<Button> buttonList = new ArrayList<>();
    private static final List<Button> slipperButtonList = new ArrayList<>();
    Optional<Integer> index = Optional.of(1);
    Optional<Integer> index2 = Optional.of(1);

    @FXML
    Button level1;
    @FXML
    Button level2;
    @FXML
    Button level3;
    @FXML
    Button level4;
    @FXML
    Button level5;
    @FXML
    Button slipper1;
    @FXML
    Button slipper2;
    @FXML
    Button slipper3;
    @FXML
    Button start;

    /**
     * Set the selected slipper.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void setSlipper(final ActionEvent event) throws IOException {
        Button button = (Button) event.getSource();
        String name = button.getText();
        for (int i = 0; i  < slipperButtonList.size(); i++) {
            slipperButtonList.get(i).setDisable(false);
        }
        button.setDisable(true);
        switch (name) { 
        case "Tiro Triplo": 
            index2 = Optional.of(1);
            break; 
        case "Tiro Perforante": 
            index2 = Optional.of(2);
            break; 
        case "Tiro Esplosivo": 
            index2 = Optional.of(3);
            break; 
        default:
            break;
        }
    }

    /**
     * Start game in the selected level.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void selectStage(final ActionEvent event) throws IOException {
        Button button = (Button) event.getSource();
        String name = button.getText();
        switch (name) { 
        case "Level 1": 
            index = Optional.of(1);
            break; 

        case "Level 2": 
            index = Optional.of(2);
            break; 

        case "Level 3": 
            index = Optional.of(3);
            break; 

        case "Level 4":
            index = Optional.of(4);
            break;

        case "Level 5":
            index = Optional.of(5);
            break;
        default:
            break;
        }
        MainWindow.getSingleton().getSoundManager().clickPlay();
        MainWindow.getSingleton().getFactory().setScene("GameViewStory.fxml");
        Scene scene = MainWindow.getSingleton().getFactory().getScene();
        scene.setOnKeyPressed(AbstractGameView.inputHandler);
        scene.setOnKeyReleased(AbstractGameView.singleShotHandler);
        Controller.get().start(index, index2);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    /**
     * Set the MainMenu scene.
     * 
     * @param event
     * @throws IOException
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        buttonList.add(level1);
        buttonList.add(level2);
        buttonList.add(level3);
        buttonList.add(level4);
        buttonList.add(level5);
        slipperButtonList.add(slipper1);
        slipperButtonList.add(slipper2);
        slipperButtonList.add(slipper3);

        for (int i = buttonList.size() - 1; i >= DataManager.getDataManager().getProgress(); i--) {
            buttonList.get(i).setDisable(true);
        }
        buttonList.clear();
    }

}
