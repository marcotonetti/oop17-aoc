package aoc.view.menu.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import aoc.controller.Controller;
import aoc.controller.datamanager.DataManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Pair;

public class FinishController  extends BasicController implements Initializable{

    @FXML
    Button confirm;
    @FXML
    TextField name;
    @FXML
    Label point;

    /**
     * back to main menu.
     * 
     * @param event
     * @throws IOException
     */

    @FXML
    private void confirm(final ActionEvent event) throws IOException {
        if (name.getText().length() >= 1 ) {
            MainWindow.getSingleton().click();
            MainWindow.getSingleton().getSoundManager().clickBackToMainMenu();
            DataManager.getDataManager().updateHighScores(new Pair<String, Integer>(name.getText(), Controller.get().getLevelPoint()));
            try {          
                sf.setScene("Highscores.fxml");
                Scene scene = sf.getScene();
                MainWindow.getSingleton().getWindow().setScene(scene);
                MainWindow.getSingleton().getWindow().show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.close();
        }
    }


    /**
     * Set the MainMenu scene.
     * 
     * @param event
     * @throws IOException
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        point.setText(String.valueOf((Controller.get().getLevelPoint())));
    }

}
