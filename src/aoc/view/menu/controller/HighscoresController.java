package aoc.view.menu.controller;

import java.net.URL;
import java.util.ResourceBundle;

import aoc.controller.datamanager.DataManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.util.Pair;

/**
 * The controller class for the highscores scene.
 * 
 */
public class HighscoresController extends BasicController implements Initializable {

    private static final ObservableList<Pair<String, Integer>> list = FXCollections.observableArrayList(DataManager.getDataManager().getHighScores());
    @FXML
    ListView<Pair<String, Integer>> arcade;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        arcade.setItems(list);
    }
}
