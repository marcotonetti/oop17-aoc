package aoc.view.menu.controller;

/**
 * Interface for SoundManager.
 *
 */
public interface SoundManager {

    /**
     * Getter for the volume.
     * 
     * @return current value of the volume
     */
    double getVolume();

    /**
     * Setter for the volume.
     * 
     * @param volume
     *          the volume desired
     */
    void setVolume(double volume);

    /**
     * Play button sound.
     */
    void click();

    /**
     * Change to game music from menu music.
     */
    void clickPlay();

    /**
     * Change to menu music from game music.
     */
    void clickBackToMainMenu();
}
