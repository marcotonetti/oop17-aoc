package aoc.view.menu.controller;

import java.io.IOException;
import java.util.Optional;

import aoc.controller.Controller;
import aoc.view.gameview.AbstractGameView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * The controller class for the SelectMode scene.
 *
 */
public class SelectModeController extends BasicController{

    /**
     * Set the SelectStage scene.
     * 
     * @param event
     * @throws IOException
     */
    @FXML
    private void playStory(final ActionEvent event) throws IOException {
        sf.setScene("SelectStage.fxml");
        this.showScene(event);
    }

    /**
     * Start game in Arcade Mode.
     * 
     * @param event
     */
    @FXML
    private void playArcade(final ActionEvent event) throws IOException  {
        MainWindow.getSingleton().getSoundManager().clickPlay();
        MainWindow.getSingleton().getFactory().setScene("GameViewArcade.fxml");
        Scene scene = MainWindow.getSingleton().getFactory().getScene();
        scene.setOnKeyPressed(AbstractGameView.inputHandler);
        scene.setOnKeyReleased(AbstractGameView.singleShotHandler);
        Controller.get().start(Optional.empty(), Optional.of(1));
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }
}
