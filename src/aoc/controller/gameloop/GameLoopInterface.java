package aoc.controller.gameloop;

import aoc.model.entity.slipper.Projectile;

/**
 * The interface for the gameloop, which implements the gameloop proxy.
 */
public interface GameLoopInterface extends GameLoopProxy {

    void receiveAndSendSpecialProjectile(final Projectile specialShoots);

    /**
     * This method returns the points of the ArcadeMode.
     * @return the points
     */
    int getPoint();

}
