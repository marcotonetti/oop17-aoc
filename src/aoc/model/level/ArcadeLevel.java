package aoc.model.level;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import aoc.model.Model.GameStatus;
import aoc.model.WorldConstants;
import aoc.model.entity.child.Child;
import aoc.model.entity.child.ChildInterface;
import aoc.model.entity.child.Children;
import aoc.model.entity.slipper.SlipperInterface;
import aoc.model.level.spawner.ChildSpawner;
import aoc.utilities.Pair;

public class ArcadeLevel extends AbstractLevel {

    /**
     * Path of the jsonfile.
     */
    private final String levelPath = "/levels/arcade.json";

    /**
     * Point of the level
     */
    private int point = 0;

    /**
     * Constructor for StoryLevel.
     * @param index
     *          The level selected.
     */
    public ArcadeLevel() {
        super();
        this.setCurrentLevel(1);
        this.setSpawner(new ChildSpawner(loadLevel(levelPath), this));
    }

    /**
     * Loads the data of all the children that need to be
     * spawned in the selected level.
     * 
     * @return all the children in form of list.
     */
    private List<Pair<Map<Children, Integer>, Double>> loadLevel(final String path) {
        List<Pair<Map<Children, Integer>, Double>> map = Collections.emptyList();
        List<Pair<Map<Children, Integer>, Double>> map2 = new LinkedList<Pair<Map<Children, Integer>, Double>>();
        try (Reader r = new InputStreamReader(this.getClass().getResourceAsStream(path))) {        
            final Gson gson = new GsonBuilder().create();
            map = gson.fromJson(r, new TypeToken<List<Pair<Map<Children, Integer>, Double>>>(){}.getType());
            map2.add(new Pair<Map<Children, Integer>, Double>(map.get(0).getFirst(), map.get(0).getSecond() * Math.pow(0.75, this.getCurrentLevel() -1 )));         
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {        
        if (this.status.equals(GameStatus.PLAYING)) {
            this.entities.stream().filter(e -> e instanceof ChildInterface && !e.isAlive()).forEach(p -> {
                this.setLevelPoint(this.getChildren((Child) p));
            });
            this.entities.removeIf(e -> !e.isAlive()
                    || e.getPosition().getX() > WorldConstants.CELL_WIDTH * WorldConstants.WORLD_WIDTH && e instanceof SlipperInterface);
            this.entities.forEach(x -> x.update());
            if(!explosiveEntities.isEmpty()) {
                this.entities.addAll(explosiveEntities);
                explosiveEntities.clear();
            }               
            if (spawner.readyToSpawn()) {
                entities.add(spawner.entityToSpawn());
            }
            final boolean res = entities.stream().noneMatch(e -> e instanceof ChildInterface
                    && e.getPosition().getX() <= WorldConstants.GAMEOVER_LINE);
            if (!res) {
                this.status = GameStatus.LOST;
            } else if (spawner.isEmpty()
                    && this.entities.stream().noneMatch(e -> e instanceof ChildInterface)) {
                this.win();
            } else {
                spawner.tick();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void win() {
        this.setCurrentLevel(getCurrentLevel() + 1);
        this.setSpawner(new ChildSpawner(loadLevel(levelPath), this));
    }

    /**
     * {@inheritDoc}
     */
    public int getLevelPoint() {
        return this.point;
    }

    /**
     * {@inheritDoc}
     */
    public void setLevelPoint(int childrenPoint) {
        this.point = this.point + childrenPoint;
    }

    /**
     * {@inheritDoc}
     */
    public int getChildren(Child c) {
        return c.getPoint();    
    }
}
