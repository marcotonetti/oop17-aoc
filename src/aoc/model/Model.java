package aoc.model;

import aoc.model.entity.slipper.Projectile;
import aoc.utilities.Direction;

public interface Model extends LevelProxy {

    /**
     * The possible states of the game currently played.
     */
    public enum GameStatus {

        /**
         * The level is being played.
         */
        PLAYING,

        /**
         * The level has been won.
         */
        WON,

        /**
         * The level has been lost.
         */
        LOST;
    }

    /**
     * The possible shooting styles.
     */
    public enum ShootingStyle {

        /**
         * Rapid Fire.
         */
        RAPID,

        /**
         * Single Shot.
         */
        SINGLE,

        /**
         * Special Shot.
         */
        SPECIAL;
    }

    /**
     * This method moves the Mother object in
     * the Direction passed as a parameter.
     * 
     * @param dir
     *            The direction the mother has to move to
     */
    void moveMother(Direction dir);

    /**
     * This method set the special arm.
     * 
     * @param Projectile 
     */
    void setSpecialShoots(Projectile specialShoots);

    /**
     * This method makes the mother shoots a projectile,
     * and it can be a single shot or a rapid fire.
     * In the second case there's a limit of projectiles
     * shot per second.
     * 
     * @param style
     *            The ShootingStyle of the projectile
     */
    void shoot(ShootingStyle style);

    /**
     * Getter for the special attack
     * 
     * @return String representation of specialAttack
     */
    String getSpecialAttack();

    /**
     * Getter for the special attack time recharge.
     * 
     * @return long time
     */
    long getTime();

    /**
     * Getter for the special attack use
     * 
     * @return int of specialAttack
     */
    int getAmmo();
}
