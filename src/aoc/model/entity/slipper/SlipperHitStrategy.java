package aoc.model.entity.slipper;

@FunctionalInterface
public interface SlipperHitStrategy {
    
    void slipperHit(Slipper slipper);

}
