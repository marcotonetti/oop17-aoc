package aoc.model.entity.slipper;

import aoc.model.entity.EntityInterface;
import aoc.utilities.ObjectObserver;
import aoc.utilities.ObservedObject;
import aoc.utilities.Vector;

/**
 * This class implements a slipper factory.
 * It mainly creates a new slipper from the informations that receives from the caller.
 */
public class SlipperFactory implements SlipperFactoryInterface,  ObservedObject{

    private ObjectObserver observer;

    /**
     * The strategy of each slipper.
     */

    private SlipperHitStrategy getNothingStrategy() {
        return (s) -> {};            
    }

    private SlipperHitStrategy getKillStrategy() {
        return (s) -> {
            s.kill();
        };            
    }

    private SlipperHitStrategy getExplosiveStrategy() {
        return (s) -> {
            s.kill();
            for(int i = 0; i < 2; i++) {
                if(i != 0) {
                    final EntityInterface f = this.createSlipper(Projectile.FRAGMENT_SLIPPER, new Vector(s.getPosition().getX(), s.getPosition().getY()));
                    f.setSpeed(new Vector(f.getSpeed().getX(), f.getSpeed().getY() * 1));
                    this.observer.notify(f);
                } else {
                    final EntityInterface f = this.createSlipper(Projectile.FRAGMENT_SLIPPER, new Vector(s.getPosition().getX(), s.getPosition().getY()));
                    f.setSpeed(new Vector(f.getSpeed().getX(), f.getSpeed().getY() * -1));
                    this.observer.notify(f);
                }
            }  
        };            
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityInterface createSlipper(final Projectile type, final Vector position) {
        SlipperHitStrategy strategy;
        switch (type) {
        case BASIC_SLIPPER:
            strategy = this.getKillStrategy();
            break;
        case TRIPLE_SLIPPER:
            strategy = this.getKillStrategy();
            break;
        case PIERCING_SLIPPER:
            strategy = this.getNothingStrategy();
            break;
        case EXPLOSIVE_SLIPPER:
            strategy = this.getExplosiveStrategy();
            break;
        case FRAGMENT_SLIPPER:
            strategy = this.getNothingStrategy();
        default:
            strategy = this.getKillStrategy();
            break;
        }
        return new Slipper(position, type, strategy);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void attach(ObjectObserver observer) {
        this.observer = observer;       
    }

}
