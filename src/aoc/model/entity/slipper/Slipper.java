package aoc.model.entity.slipper;

import aoc.utilities.Vector;
import aoc.model.WorldConstants;
import aoc.model.entity.Entity;

/**
 * This class is the basic implementation for a slipper.
 */
public class Slipper extends Entity implements SlipperInterface {

    /**
     * The projectile type.
     */
    private Projectile type;

    /**
     * The damage dealt by the slipper.
     */
    private final int damage;

    /**
     * The strategy of the slipper.
     */
    private SlipperHitStrategy strategy;

    /**
     * The constructor for the slipper.
     * @param position
     *             The initial position of the slipper.
     * @param projectile
     *             The projectile type of the slipper, which will provide the initial informations required by the superclass' constructor.
     */
    public Slipper(final Vector position, final Projectile projectile, final SlipperHitStrategy strategy) {
        super(position, new Vector(projectile.getXSpeed(), projectile.getYSpeed()), projectile.name());
        this.damage = projectile.getDamage();
        this.strategy  = strategy;
        this.type = projectile;
    }

    /**
     * {@inheritDoc}
     */
    public Projectile getType() {
        return this.type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hit() {
        strategy.slipperHit(this);
        return this.damage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        super.update();
        if (this.getPosition().getY() < WorldConstants.ROW_CENTERS.get(0) 
                || this.getPosition().getY() > WorldConstants.ROW_CENTERS.get(WorldConstants.WORLD_HEIGHT - 1)) {
            this.setSpeed(new Vector(this.getSpeed().getX(), this.getSpeed().getY() * -1));
        }
    }

}
