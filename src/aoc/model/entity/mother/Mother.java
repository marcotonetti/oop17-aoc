package aoc.model.entity.mother;

import aoc.utilities.Direction;
import aoc.utilities.ObjectObserver;
import aoc.utilities.ObservedObject;
import aoc.utilities.Vector;
import aoc.model.entity.slipper.Projectile;
import aoc.model.entity.slipper.SlipperFactory;
import aoc.model.entity.slipper.SlipperFactoryInterface;

import aoc.controller.GameConstants;
import aoc.model.WorldConstants;
import aoc.model.entity.Entity;
import aoc.model.entity.EntityInterface;

/**
 * This class implements the mother, which is the character controlled by the player.
 */
public class Mother extends Entity implements MotherInterface, ObservedObject, ObjectObserver {

    /**
     * This field represents the recharging time of the specialProjectile shot by the mother during the game.
     */
    private static final long RECHARGE_TIME = GameConstants.UPS * 4;

    /**
     * This field represents the type of projectile shot by the mother during the game.
     */
    private final Projectile shoots;

    /**
     * This field represents the type of specialProjectile shot by the mother during the game.
     */
    private Projectile specialShoots;

    /**
     * This field represents the usage of specialProjectile shot by the mother during the game.
     */
    private int count = 3;

    /**
     * This field represents the time to  recharge of specialProjectile shot by the mother during the game.
     */
    private long time = 0;

    /**
     * This field contains the reference to the slipper factory which creates 
     * the slippers shot by the mother during the game.
     */
    private final SlipperFactoryInterface slipperFactory;

    /**
     * This field contains the reference to the observer of the mother.
     */
    private ObjectObserver observer;

    /**
     * The constructor of the mother.
     * @param position
     *             The initial position of the mother.
     * @param projectile
     *             The projectile which the mother will throw during all the game.
     */
    public Mother(final Vector position, final Projectile projectile, final Projectile specialProjectile ) {
        super(position, new Vector(0, 0), "MOTHER");
        this.shoots = projectile;
        this.specialShoots = specialProjectile;
        this.slipperFactory = new SlipperFactory();
        slipperFactory.attach(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EntityInterface attack() {
        final EntityInterface thrown = this.slipperFactory.createSlipper(shoots, new Vector(this.getPosition().getX(), this.getPosition().getY()));
        this.observer.notify(thrown);
        return thrown;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void move(final Direction direction) {
        this.setPosition(this.getPosition().increaseY(direction.getDir() * WorldConstants.CELL_WIDTH));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void attach(final ObjectObserver observer) {
        this.observer = observer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void specialAttack() {
        if(count > 0){
            EntityInterface thrown = this.slipperFactory.createSlipper(specialShoots, new Vector(this.getPosition().getX(), this.getPosition().getY()));
            this.observer.notify(thrown);
            if(specialShoots.equals(Projectile.TRIPLE_SLIPPER)) {
                final int motherRow = WorldConstants.ROW_CENTERS.indexOf(this.getPosition().getY());

                if (motherRow < WorldConstants.WORLD_HEIGHT - 1) {               
                    thrown = this.slipperFactory.createSlipper(specialShoots, new Vector(this.getPosition().getX(), this.getPosition().getY() + WorldConstants.CELL_WIDTH ));
                    this.observer.notify(thrown);
                } if (motherRow > 0) {
                    thrown = this.slipperFactory.createSlipper(specialShoots, new Vector(this.getPosition().getX(), this.getPosition().getY() - WorldConstants.CELL_WIDTH));
                    this.observer.notify(thrown);
                }
            }
            count--;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSpecialShoots(Projectile specialShoots) {
        this.specialShoots = specialShoots;   
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        super.update();
        if(count == 0) {
            time++;  
        }
        if(time >= RECHARGE_TIME) {
            time = 0;
            count = 3;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSpecialShoots() {
        return this.specialShoots.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getTime() {
        return this.time;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getAmmo() {
        return this.count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void notify(Object notifier) {
        this.observer.notify(notifier);
    }

}
