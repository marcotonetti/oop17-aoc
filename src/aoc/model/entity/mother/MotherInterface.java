package aoc.model.entity.mother;

import aoc.utilities.Direction;
import aoc.utilities.ObservedObject;

import aoc.model.entity.EntityInterface;
import aoc.model.entity.slipper.Projectile;

/**
 * This interface describes the entity of the mother.
 * She has a method for attack, which means she shots a new slipper,
 * and a method for moving, which requires the direction.
 */
public interface MotherInterface extends EntityInterface, ObservedObject {

    /**
     * Returns the reference to the new slipper shot.
     * 
     * @return the slipper shot.
     */
    EntityInterface attack();

    /**
     * Orders to the mother to move of one step in the specified direction.
     * 
     * @param direction
     *            the direction of movement.
     */
    void move(Direction direction);

    /**
     * Setter for the specialShoots
     * 
     * @param specialShoots
     */
    void setSpecialShoots(Projectile specialShoots);

    /**
     * Getter for the specialShoots
     * 
     * @return String representation of specialShoots
     */
    String getSpecialShoots();

    /**
     * Returns the reference to the recharge time of the specialSlipper shot.
     * 
     * @return long time.
     */
    long getTime();

    /**
     * Returns the usage of the specialSlipper shot.
     * 
     * @return int usage.
     */
    int getAmmo();

    /**
     * Returns the reference to the new specialSlipper shot.
     * 
     * @return the slipper shot.
     */
    void specialAttack();

}
