package aoc.utilities;

/**
 * This enumeration list all the inputs that the Controller must handle to play the game.
 */
public enum Input {

    /**
     * This value represents the single shot of a projectile.
     */
    SHOT,

    /**
     * This value represents the single shot of a specialProjectile.
     */
    SPECIAL_SHOT,

    /**
     * This value represents the alternative fire mode.
     */
    RAPID_SHOT,

    /**
     * This value represents a single move of the mother in the up direction.
     */
    UP,

    /**
     * This value represents a single move of the mother in the down direction.
     */
    DOWN,

    /**
     * This value represents a single move of the mother in the down direction.
     */
    PAUSE;
}
